## Project Description
This project is created for i2x frontend coding challenge.

## Project Setup
Run `git clone https://gitlab.com/noumanwaheed/i2x-frontend-challenge.git`

Run `cd i2x-frontend-challenge`

Run `yarn install`

Run `yarn run start`

Visit URL `http://localhost:3000`

## Project Testing
Run `yarn run test` to run basic tests to make sure components renders correctly.

## Some Testing Techniques Could Be Performed
-`Jest` for compile time rendering exceptions.

-`Enzyme` with shallow rendering OR JSDOM For Unit Testing.

-`Selenium` could help as well to mimic the behaviour. We can play audio file to get speech input.


### Demo

https://noumanwaheed.com/i2x-frontend-challenge/
import './StateComponent.css';
import React, {
    Component,
} from 'react';
import TimeAgo from 'react-timeago'

class StateComponent extends Component {
    render() {
        return (
            <p>
                <span>Status:</span>&nbsp;<span className={ "badge badge-" + this.props.currentState.class} >Session { this.props.currentState.label } </span><small>&nbsp;<TimeAgo date={ this.props.startTime } /></small><small>&nbsp;{ this.props.error }</small>
            </p>
        );
    }
}

export default StateComponent;

import React from 'react';
import ReactDOM from 'react-dom';
import StateComponent from './StateComponent';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<StateComponent startTime='' error='' currentState={{id: 0, label: 'Awaiting', class: 'dark'}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

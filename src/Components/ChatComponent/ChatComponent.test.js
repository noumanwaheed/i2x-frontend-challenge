import React from 'react';
import ReactDOM from 'react-dom';
import ChatComponent from './ChatComponent';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ChatComponent chat={[]}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

import './ChatComponent.css';
import React, {
    Component,
} from 'react';
import TimeAgo from 'react-timeago'

class ChatComponent extends Component {
    render() {
        return (
            <div className="card shadow-sm mb-3 chatbox">
                <div className="card-body text-light">
                    {
                        this.props.chat.map((bubble, index) => {
                            let content =  bubble.words.map( (word, subIndex) => {
                                return bubble.spotted.includes(word)? (<small className="badge badge-pill badge-warning" key={subIndex}>{ word }</small>): word
                            });
                            return (<div className="mb-3 text-right" key={index}><p className="text-left mb-0 card-text p-3 bg-dark rounded shadow font-italic">{ content }</p><small className="text-right text-dark"><TimeAgo date={ bubble.utc } /></small></div>);
                        })
                    }
                </div>
            </div>
        );
    }
}

export default ChatComponent;

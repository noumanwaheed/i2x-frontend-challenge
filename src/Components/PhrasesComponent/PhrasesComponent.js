import './PhrasesComponent.css';
import React, {
    Component,
} from 'react';

class PhrasesComponent extends Component {
    render() {
        return (
            <div className="card shadow-sm mb-3">
                <div className="card-body">
                    <textarea rows="10" className="w-100 border-0" value={this.props.phrases.join('\n')} onChange={this.props.onChange}/>
                </div>
            </div>
        );
    }
}

export default PhrasesComponent;

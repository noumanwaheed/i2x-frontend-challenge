import React from 'react';
import ReactDOM from 'react-dom';
import PhrasesComponent from './PhrasesComponent';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PhrasesComponent phrases={['product', 'Hi', 'Hello', 'My name is']} onChange={()=>{}}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

import './App.css';
import React, {
  Component,
  Fragment
} from 'react';
import { ASRClient } from './asr/ASRClient';
import { compact } from 'lodash';
import moment from 'moment';
import ChatComponent from "./Components/ChatComponent/ChatComponent";
import StateComponent from "./Components/StateComponent/StateComponent";
import PhrasesComponent from "./Components/PhrasesComponent/PhrasesComponent";

const states = {
  awaiting: {
    id: 0,
    label: 'Awaiting',
    class: 'dark'
  },
  started: {
    id: 1,
    label: 'Started',
    class: 'success'
  },
  stopped: {
    id: 2,
    label: 'Disconnected',
    class: 'warning'
  },
  failed: {
    id: -1,
    label: 'Error',
    class: 'danger'
  }
};


class App extends Component {
  ASRInstance = new ASRClient('wss://vibe-rc.i2x.ai');

  state = {
    currentState: states.awaiting,
    startTime: '',
    phrases: ['product', 'Hi', 'Hello', 'My name is'],
    error: '',
    chat: [],
  };

  _handleError = (error) => {
    this.setState({ currentState: states.failed });
    this.setState({ error: error});
    this.setState({ startTime:  ''} );
  };

  componentDidCatch = (error, info) => {
    this._stopSession();
    this.setState({error: error.message.substr(error.message.indexOf('('),(error.message.indexOf(')')-error.message.indexOf('('))+1).replace('found: ','')});
    this.setState({ currentState: states.failed });
    this.setState({ startTime:  ''} );
  };

  _startSession = () => {
    try {
      this.ASRInstance.start(compact(this.state.phrases), this._onMessage);
      this.setState({ currentState: states.started });
      this.setState({ startTime:  moment().valueOf()} );
    }
    catch(error){
      this._handleError(error);
    }
  };

  _stopSession = () => {
    try {
      this.ASRInstance.stop();
      this.setState({ currentState: states.stopped });
      this.setState({ startTime:  ''} );
    }
    catch(error){
      this._handleError(error);
    }
  };

  _onToggle = () => {
    if (this.state.currentState === states.started) {
      this._stopSession();
    } else {
      this._startSession();
    }
  };

  _onUpdatePhrases = (event) => {
    try {
      const nextPhrases = event.target.value.split('\n');
      if (this.ASRInstance.isStarted()) {
        this.ASRInstance.updateSpottingConfig(compact(nextPhrases));
      }
      this.setState({phrases: nextPhrases});
    }
    catch(error){
      this._handleError(error);
    }
  };

  _onMessage = (error, results) => {
    try {
      let tempChat = this.state.chat;
      let diff = (tempChat.length > 0) ? moment().diff(tempChat[0].utc, 'seconds') : -1;
      if (diff <= 2 && diff > -1) {
        tempChat[0].words = tempChat[0].words.concat([
          <br/>]).concat(this.breakWords(results.transcript.utterance, results.spotted));
        tempChat[0].spotted = tempChat[0].spotted.concat(results.spotted);
      }
      else
        tempChat.unshift({
          words: this.breakWords(results.transcript.utterance, results.spotted),
          spotted: results.spotted,
          utc: moment().valueOf()
        });
      this.setState({chat: tempChat});
    }
    catch(error){
      this._handleError(error);
    }
  };

  breakWords = (str, matchArr) => {
    for(let match of matchArr) {
      let regex = new RegExp(match, 'i');
      str = str.replace(regex, `@${match}@`);
    }
    let words = str.split('@');
    if(words[0] === '')
      words.splice(0,1);
    if(words[words.length-1] === '')
      words.splice(words.length-1,1);
    return words;
  };

  render() {
    return (
        <Fragment>
          <div className="container mt-4">
            <div className="row m-0">
              <StateComponent startTime={this.state.startTime} error={this.state.error} currentState={this.state.currentState}/>
            </div>
            <div className="row">
              <div className="col-8">
                <p><strong>Transcript</strong></p>
                <ChatComponent chat={this.state.chat} states={states}/>
                <button type="button" className="btn btn-dark" onClick={this._onToggle}>{this.state.currentState === states.started ? 'Stop' : 'Start'} Session</button>
              </div>
              <div className="col-4">
                <p><strong>Spotting Phrases</strong></p>
                <PhrasesComponent phrases={this.state.phrases} onChange={this._onUpdatePhrases}/>
              </div>
            </div>
          </div>
        </Fragment>
    );
  }
}

export default App;

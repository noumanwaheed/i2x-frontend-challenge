#!/bin/sh
set -xe

# now=`date '+%Y%m%dT%H%M%S'`;

# buildDir=${now}

buildDir='i2x'

scp -r ./build ubuntu@18.220.246.168:~/${buildDir}

ssh -t ubuntu@18.220.246.168 << EOF

    cd ./${buildDir}
    
    echo "Shutting down old version"
    
    sudo docker-compose down
    
    echo "Installing new version..."
    
    echo "  Building the version..."
    
    sudo docker-compose build --no-cache
    
    echo "  Version is build."

    echo "  Starting the version..."
    
    sudo docker-compose up -d
    
    echo "Version is installed and started."
EOF

#!/bin/sh
# Change to extended shell
set -xe

# remove build directory
rm -rf build

# build app
yarn install
yarn run build


# copy any relevant files to build folder
cp -r \
    deploy/docker/docker-compose.yml \
    deploy/docker/Dockerfile \
    deploy/docker/nginx \
    build
